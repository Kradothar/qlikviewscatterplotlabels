var extensionName = "Scatter";


Qv.AddExtension(extensionName,
	function () {
		// work with the extension as added
		var _this = this;

		initAll(0, 1);
		
		function initAll(showReg, fullLabels){
			// work with the extension as added
			_this.ExtSettings = {};
			_this.ExtSettings.ExtensionName = extensionName;
			_this.ExtSettings.extensionPath = Qva.Remote + "?public=only&name=Extensions/" + extensionName + "/";
			
			// General Settings
			_this.ExtSettings.UniqueId = _this.Layout.ObjectId.replace("\\", "_");
			
			// Build a collection of css Files
			// Base Url for CSS Files
			_this.ExtSettings.LoadUrl = Qva.Remote + (Qva.Remote.indexOf('?') >= 0 ? '&' : '?') + 'public=only' + '&name=';
				
			var cssFiles = [];
			
			cssFiles.push(_this.ExtSettings.extensionPath + 'lib/style.css');
			for (var i = 0; i < cssFiles.length; i++) {
				Qva.LoadCSS(cssFiles[i]);
			}
			
			// Build a collection of include files
			var jsFiles = [];

			jsFiles.push(_this.ExtSettings.extensionPath + 'lib/d3.min.js');
			jsFiles.push(_this.ExtSettings.extensionPath + 'lib/scatter.js');
			jsFiles.push(_this.ExtSettings.extensionPath + 'lib/labeler.js');
			jsFiles.push(_this.ExtSettings.extensionPath + 'lib/colorbrewer.min.js'); // reduced colorBrewer set for palettes

			Qv.LoadExtensionScripts(jsFiles, function () {
			
				// Initialise incoming data
				initData();
				initCanvas(showReg, fullLabels);
				initChart(showReg, fullLabels);
			});
		}
		// ------------------------------------------------------------------
		// Initializes the data
		// ------------------------------------------------------------------		
		function initData (){
            var jsonData = [];
			_this.ExtData = [];
			for (var i = 0; i < _this.Data.Rows.length; i++){
                var name = _this.Data.Rows[i][0].text;
				
				// Turn our expressions into numbers
                var x_value = +_this.Data.Rows[i][1].text;
                var y_value = +_this.Data.Rows[i][2].text;
                var tooltip = _this.Data.Rows[i][3].text;
				jsonData[i] = {};
				jsonData[i].name = name;
				jsonData[i].x = x_value;
				jsonData[i].y = y_value;
				jsonData[i].tooltip = tooltip;
			}			
            _this.ExtData = jsonData;
		}

		// ------------------------------------------------------------------
		// Initializes the Divs into which we are placing our D3 objects
		// ------------------------------------------------------------------
		function initCanvas(showReg, fullLabels) {

			$(_this.Element).empty();
			
			// Store the id of the chart div into a global
			_this.ExtSettings.div_canvas = 'Chart_' + _this.ExtSettings.UniqueId;
			
			
			var $divContainer = $(document.createElement("div"));

			var ctrlPanel = initCtrl(showReg, fullLabels);
			$(_this.Element).append(ctrlPanel);
			
			var $chart = $(document.createElement("div"));
			$chart.attr('id', _this.ExtSettings.div_canvas);
			$chart.addClass('chart');
			$divContainer.append($chart);
			$divContainer.attr("height", "1000") ;
			
			$(_this.Element).append($divContainer);
			
			
		}	
		// Control Panel container. For now this absolutely aligns to the base of the extension
		function initCtrl(showReg, fullLabels){
	
			// Function to recall stack
			var checkedFn = function () {
				initAll($("#ctrlGroupRegression :checkbox:checked").length, $("#ctrlGroupLabelStyle :checkbox:checked").length );		
			}
			
			var ctrlPanel = $(document.createElement("div"));
			ctrlPanel.attr("id", "ctrlPanel");
			ctrlPanel.addClass("ctrlP");
	
			//////////////////////////////////////////////////////////////////////
			// Controller container for linear regression checkbox and label
			var ctrlGroupRegression = $(document.createElement("fieldSet"));
			ctrlGroupRegression.attr("id", "ctrlGroupRegression");
			ctrlGroupRegression.addClass("ctrlGroup");
	
			// Set up the Checkbox for linear regression
			// First the label
			var label =  $(document.createElement("label"));
			label.htmlFor = "checkbox_linear";
			label.append($(document.createTextNode('Show linear regression')));
			
			
			// Next the checkbox
			var checkbox = $(document.createElement("input"));
			checkbox.attr("type", "checkbox");
			checkbox.attr("id", "checkbox_linear");
			checkbox.attr("name", "checkbox_linear");
			checkbox.prop("checked", showReg);
			
			
			checkbox.click(checkedFn);
			
			ctrlGroupRegression.append (checkbox); 
			ctrlGroupRegression.append (label); 
			
			//////////////////////////////////////////////////////////////////////
			// Controller container for Label style checkbox and label 
			// toggles between full labels and reduced size labels
			var ctrlGroupLabel = $(document.createElement("fieldSet"));
			ctrlGroupLabel.attr("id", "ctrlGroupLabelStyle");
			ctrlGroupLabel.addClass("ctrlGroup");
	
			// Set up the Checkbox for linear regression
			// First the label
			label =  $(document.createElement("label"));
			label.htmlFor = "checkbox_label";
			label.append($(document.createTextNode('Show full labels')));
			
			
			// Next the checkbox
			checkbox = $(document.createElement("input"));
			checkbox.attr("type", "checkbox");
			checkbox.attr("id", "checkbox_label");
			checkbox.attr("name", "checkbox_label");
			checkbox.prop("checked", fullLabels);
			
			checkbox.click(checkedFn);
			
			ctrlGroupLabel.append (checkbox); 
			ctrlGroupLabel.append (label); 
			
			// Append the full label checkbox
			ctrlPanel.append(ctrlGroupRegression, ctrlGroupLabel);
			
			
			
			return ctrlPanel;
			
			//$(_this.Element).append(ctrlPanel);
			
		}
		
		function initChart (showReg, fullLabels){
			var margin = {top: 20, right: 20, bottom: 30, left: 40},
			width = 970 - margin.left - margin.right,
			height = 500 - margin.top - margin.bottom;

            // Set up the scatterplot canvas
			scatterplot.defineLayout(_this.ExtSettings.div_canvas, height, width, 10, margin);

			
            _this.ExtSettings.LabelXAxis = _this.Layout.Text0.text;				
            _this.ExtSettings.LabelYAxis = _this.Layout.Text1.text;
				
			// Draw the canvas
            scatterplot.draw(_this.ExtData);
			
			// Axes
			scatterplot.axes(_this.ExtSettings.LabelXAxis, _this.ExtSettings.LabelYAxis);
			// Add labels
			scatterplot.label(_this.ExtData, fullLabels);
			
			// Spread out the labels
			scatterplot.spreadLabels();
			
			// Add average lines
			scatterplot.averageX();
			scatterplot.averageY();
			
			if(showReg){
				scatterplot.regressionLine(_this.ExtData);
			}
			
		}
		
		// ------------------------------------------------------------------
		// Extension helper functions
		// ------------------------------------------------------------------
		function ConsoleLog(msg) {
			if (typeof console != "undefined") {
				console.log(msg);
			}
		}
		function ConsoleInfo(msg) {
			if (typeof console != "undefined") {
				console.info(msg);
			}
		}
		function ConsoleClear() {
			if (typeof console != "undefined") {
				console.clear();
			}
		}
		
	}
);