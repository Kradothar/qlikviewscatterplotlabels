scatterplot = {
	_s: {},
	svg: {},
	margin: 0,
	height: 0,
	width: 0,
	r: 0,
	tooltip: {},
	// Create the svg  canvas given the containing DIV and create it, also setting up all the size constants
	defineLayout: function (div_canvas, height, width, radius, margin){
		// Copy the base scatterplot into _s to avoid scope contention of the this variable within embedded anonymous functions
		_s = this;
		_s.margin = margin;
		_s.height = height;
		_s.width = width;
		_s.r = radius;
		
		// add the graph canvas to the body of the webpage
		var canvas = d3.select("#" + div_canvas);
		
		_s.svg = canvas.append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
						
		// add the tooltip area to the webpage
		_s.tooltip = canvas.append("div")
			.attr("class", "tooltip")
			.style("opacity", 0);
				
	},
	// Draw the elements onto the canvas
	draw: function(data){
		// setup x 
		_s.xValue = function(d) { return d.x;}; // data -> value
		_s.xScale = d3.scale.linear().range([0, _s.width]); // value -> display
		_s.xMap = function(d) { return _s.xScale(_s.xValue(d));}; // data -> display
		_s.xAxis = d3.svg.axis().scale(_s.xScale).orient("bottom");

		// setup y
		_s.yValue = function(d) { return d.y;}; // data -> value
		_s.yScale = d3.scale.linear().range([_s.height, 0]); // value -> display
		_s.yMap = function(d) { return _s.yScale(_s.yValue(d));}; // data -> display
		_s.yAxis = d3.svg.axis().scale(_s.yScale).orient("left");

		// setup fill color
		var cValue = function(d) { return 1;},
			color = d3.scale.category10();
	
		var tooltip = _s.tooltip;
		
		// don't want dots overlapping axis, so add in buffer to data domain
		var xMin = d3.min(data, _s.xValue) - 1;
		var xMax = d3.max(data, _s.xValue) + 1;
		  
		_s.xScale.domain([xMin, xMax]);
		_s.yScale.domain([d3.min(data, _s.yValue) - 1, d3.max(data, _s.yValue) + 1]);

		var svg = _s.svg; 
			  
		this.defineColours();
		
		  // draw dots
		svg.selectAll(".dot")
			  .data(data)
			.enter().append("circle")
			  .attr("class", "dot")
			  .attr("r", _s.r)
			  .attr("cx", _s.xMap)
			  .attr("cy", _s.yMap)
			  .attr('fill', function (d,i){ return 'url(#gradient' + (i % colorbrewer.length) + ')';});
/*
		  // draw legend
		  var legend = svg.selectAll(".legend")
			  .data(color.domain())
			.enter().append("g")
			  .attr("class", "legend")
			  .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

		  // draw legend colored rectangles
		  legend.append("rect")
			  .attr("x", _s.width - 18)
			  .attr("width", 18)
			  .attr("height", 18)
			  .style("fill", color);

		  // draw legend text
		  legend.append("text")
			  .attr("x", _s.width - 24)
			  .attr("y", 9)
			  .attr("dy", ".35em")
			  .style("text-anchor", "end")
			  .text(function(d) { return d;});
		  */
		},
		axes: function(xLabel, yLabel){
			// x-axis
			_s.svg.append("g")
				  .attr("class", "x axis")
				  .attr("transform", "translate(0," + _s.height + ")")
				  .call(_s.xAxis)
				.append("text")
				  .attr("class", "label")
				  .attr("x", _s.width)
				  .attr("y", -6)
				  .style("text-anchor", "end")
				  .text(xLabel);

			  // y-axis
			_s.svg.append("g")
				  .attr("class", "y axis")
				  .call(_s.yAxis)
				.append("text")
				  .attr("class", "label")
				  .attr("transform", "rotate(-90)")
				  .attr("y", 6)
				  .attr("dy", ".71em")
				  .style("text-anchor", "end")
				  .text(yLabel);
		},
		label : function (data, fullLabels){
			// create label array and anchor array
			// Loop through data and apply transform
			// Also apply string transform for reduced labels

			this.makeLabelArr(data, fullLabels)
			
			// Draw labels
			_s.labels = _s.svg.selectAll(".label")
				.data(_s.label_array, function(d, i) { return d + i; })
				.enter()
				.append("text")
				.attr("class", "label")
				.attr('text-anchor', 'start')
				.text(function(d) { return d.name; })
				//.attr("x", function(d) { return (_s.xMap); })
				.attr("x", function(d) { return d.x ; })
				.attr("y", function(d) { return d.y ; })
				.attr("fill", function (d,i){ return 'url(#gradient' + (i % colorbrewer.length) + ')';});

			// Draw links
			_s.links = _s.svg.selectAll(".link")
				.data(_s.label_array)
				.enter()
				.append("line")
				.attr("class", "link")
				.attr("x1", function(d) { return (d.x); })
				.attr("y1", function(d) { return (d.y); })
				.attr("x2", function(d) { return (d.x); })
				.attr("y2", function(d) { return (d.y); })
				.attr("stroke-width", 0.6)
				.attr("stroke", "gray");

				
			// Size of each label
			i = 0;
			_s.labels.each(function() {
				_s.label_array[i].width = this.getBBox().width;
				_s.label_array[i].height = this.getBBox().height;
				i += 1;
			});	
			
		},
		// Transform the data into collections which the label placement algorithm can work with
		// Whilst we're there transform the label names and store to array if box is unchecked
		makeLabelArr : function (data, fullLabels){
			var i;
			_s.label_array = [];
			_s.anchor_array = [];
			_s.name_array = [];
			_s.sumX = 0;
			_s.sumY = 0;

			for (i=0; i< data.length; i++){
				if(data[i]){
					var x = _s.xScale(data[i].x);
					var y = _s.yScale(data[i].y);
					var name = data[i].name;
					
					// transform label names if required
					if(!fullLabels){
						// This works for strings or underscores as word delimiter
						if (name.indexOf(' ')> -1){
							name = name.split(' ').map(function (s) { return s.charAt(0); }).join(' ');							
						}
						else if (name.indexOf('_') > -1){
							
							name = name.split('_').map(function (s) { return s.charAt(0); }).join('_');
						}
						else{
							name = name.charAt(0);
						}
						_s.name_array.push ({fullname: data[i].name, name: name});
					}
					
					_s.label_array.push({ x: x, 
											y: y, 
											name: name,
											r: _s.r});
					_s.anchor_array.push({ x: x,
											 y: y,
											 r: _s.r});
					// might as well do average calculation whilst we are iterating
					_s.sumX += x;
					_s.sumY += y;						 
				}
			}
			
			_s.avgX = _s.sumX / data.length ;
			_s.avgY = _s.sumY / data.length ;
						
		},
		// First potential function for spreading out the labels
		// from here: https://github.com/tinker10/D3-Labeler
		// Probably the best option as it handles both horizontal and vertical spacing, and is actually the fastest algorithm
		spreadLabels : function (){
/*		Debugging code - what is the current state of the label collection
			var i;
			var lab = "before:\n";
			for (i=0; i<_s.label_array.length; i++){
				lab = lab + "x: " + _s.label_array[i].x + "  y: " + _s.label_array[i].y + "  name: " + _s.label_array[i].name + "  width: " + _s.label_array[i].width + "  height: " + _s.label_array[i].height + "\n"
			}
alert(lab);
		 lab = "anchor:\n";
			for (i=0; i<_s.label_array.length; i++){
				lab = lab + "x: " + _s.anchor_array[i].x + "  y: " + _s.anchor_array[i].y + "  radius: " + _s.anchor_array[i].r + "  width: " + _s.anchor_array[i].width + "  height: " + _s.anchor_array[i].height + "\n"
			}
alert(lab);
*/
			var labeler = d3.labeler()
					.label(_s.label_array)
					.anchor(_s.anchor_array)
					.width(_s.width)
					.height(_s.height)
					.start(1000);
			this.redrawLabels();
/*
			lab = "after:\n";
			for (i=0; i<_s.label_array.length; i++){
				lab = lab + "x: " + _s.label_array[i].x + "  y: " + _s.label_array[i].y + "  name: " + _s.label_array[i].name + "  width: " + _s.label_array[i].width + "  height: " + _s.label_array[i].height + "\n"
			}
alert(lab);
*/
		},
		redrawLabels: function () {
			// Redraw labels and leader lines

			_s.labels
			.transition()
			.duration(800)
			.attr("x", function(d) { return (d.x); })
			.attr("y", function(d) { return (d.y); });

			_s.links
			.transition()
			.duration(800)
			.attr("x2",function(d) { return (d.x); })
			.attr("y2",function(d) { return (d.y); });
		},
		
		// Second potential function, borrowed from here: http://bl.ocks.org/larskotthoff/11406992
		// Designed for map label placement
		// Only seems to affect horizontal placement, and is incredibly slow - use with caution
		spreadLabels2: function () {
			var move = 1;
			while(move > 0) {
				move = 0;
				_s.svg.selectAll(".label")
				   .each(function() {
					 var that = this,
						 a = this.getBoundingClientRect();

						_s.svg.selectAll(".label")
						.each(function() {
							if(this != that) {
								var b = this.getBoundingClientRect();
								if((Math.abs(a.left - b.left) * 2 < (a.width + b.width)) &&
								   (Math.abs(a.top - b.top) * 2 < (a.height + b.height))) {
									// overlap, move labels
										var dx = (Math.max(0, a.right - b.left) +
												Math.min(0, a.left - b.right)) * 0.01,
											dy = (Math.max(0, a.bottom - b.top) +
												Math.min(0, a.top - b.bottom)) * 0.02,
											tt = d3.transform(d3.select(this).attr("transform")),
											to = d3.transform(d3.select(that).attr("transform"));
										move += Math.abs(dx) + Math.abs(dy);
						
										to.translate = [ to.translate[0] + dx, to.translate[1] + dy ];
										tt.translate = [ tt.translate[0] - dx, tt.translate[1] - dy ];
										d3.select(this).attr("transform", "translate(" + tt.translate + ")");
										d3.select(that).attr("transform", "translate(" + to.translate + ")");
										a = this.getBoundingClientRect();
								}
							}
						});
					});
			  }
		},
		
		// Third and most simple function, uses a concept called constraint relaxing 
		// pushes the labels apart from each other until all are in clear space
		// from here: http://bl.ocks.org/thudfactor/6688739
		// Faster than the second, it isn't 100% accurate - only doing vertical placement
		// Still plenty of horizontal collision going on
		spreadLabels3: function (){
			var i;
			
			again = true;
			spacingX = 40;
			spacingY = 12;
			step = .5;
			while(again) {
				again = false
				_s.label_array.forEach(function(a,index) {
					_s.label_array.slice(index+1).forEach(function(b) {
						// We're always moving along the Y axis, but
						// we need to check the X-axis space, otherwise
						// we end up with only one label occupying a 
						// y+space rank on the chart, and things
						// get spread out pretty quickly.
						dy = a.y - b.y;
						dx = a.x - b.x;
						
						if(Math.abs(dy) < spacingY && Math.abs(dx) < spacingX) {
							sign = (dy > 0) ? 1 : -1;
							deltaPos = sign*step;
							a.y += deltaPos;
							b.y -= deltaPos;
							again = true;
						}
					});
				});
			}
			this.redrawLabels();
		},
		averageX : function (){
			// Draw Averge X line
			_s.links = _s.svg.append("line")
				.attr("class", "xLine")
				.attr("x1", _s.avgX)
				.attr("y1", 0)
				.attr("x2", _s.avgX)
				.attr("y2", _s.height)
				.attr("stroke-width", 0.6)
			    .style("stroke-dasharray", ("3, 3"))
				.attr("stroke", "gray");
		},
		averageY : function(){
			// Draw Averge Y line
			_s.links = _s.svg.append("line")
				.attr("class", "xLine")
				.attr("x1", 0)
				.attr("y1", _s.avgY)
				.attr("x2", _s.width)
				.attr("y2", _s.avgY)
				.attr("stroke-width", 0.6)
			    .style("stroke-dasharray", ("3, 3"))
				.attr("stroke", "gray");
			
		},
		defineColours : function(){
			var i;
			
			for (i=0; i < colorbrewer.length; i++){
				var gradient = _s.svg.append("svg:defs")
					.append("svg:radialGradient")
					.attr("id", "gradient" + i)
					.attr("r", "80%")
					.attr("cx", "30%")
					.attr("cy", "40%")
					.attr("spreadMethod", "pad");

				// Define the gradient colors
				gradient.append("svg:stop")
					.attr("offset", "0%")
					.attr("stop-color", "#ffffff")
					.attr("stop-opacity", 1);

				gradient.append("svg:stop")
					.attr("offset", "50%")
					.attr("stop-color", colorbrewer[i])
					.attr("stop-opacity", 1);
					

				gradient.append("svg:stop")
					.attr("offset", "100%")
					.attr("stop-color", colorbrewer[i])
					.attr("stop-opacity", 1);
			}
		},
		// The code for this linear regression calculation was taken from here:
		// http://bl.ocks.org/benvandyke/8459843
		regressionLine: function (data){
			var sortFn = function(a,b){ return a-b};
			// pull out arrays of just our x values and y values
			var xSeries = data.map(function(d){return _s.xScale(d.x)}).sort(sortFn);
			var ySeries = data.map(function(d){return _s.yScale(d.y)}).sort(sortFn);
			
			var leastSquaresCoeff = this.leastSquares(xSeries, ySeries);
			
			// apply the results of the least squares regression
			var x1 = xSeries[0];
			var y1 = leastSquaresCoeff[0] * x1 + leastSquaresCoeff[1];
			var x2 = xSeries[xSeries.length - 1];
			var y2 = leastSquaresCoeff[0] * x2 + leastSquaresCoeff[1];
			var trendData = [[x1,y1,x2,y2]];
			
			var trendline = _s.svg.selectAll(".trendline")
				.data(trendData);
				
			trendline.enter()
				.append("line")
				// .attr("class", "trendline")
				.attr("x1", function(d) { return d[0]; })
				.attr("y1", function(d) { return d[1]; })
				.attr("x2", function(d) { return d[2]; })
				.attr("y2", function(d) { return d[3]; })
				.attr("stroke", "black")
				.attr("stroke-width", 1);
		},		
		
		// returns slope, intercept and r-square of the line
		leastSquares: function (xSeries, ySeries) {
			var reduceSumFunc = function(prev, cur) { return prev + cur; };
			var xBar = xSeries.reduce(reduceSumFunc) * 1.0 / xSeries.length;
			var yBar = ySeries.reduce(reduceSumFunc) * 1.0 / ySeries.length;

			var ssXX = xSeries.map(function(d) { return Math.pow(d - xBar, 2); })
				.reduce(reduceSumFunc);
			
			var ssYY = ySeries.map(function(d) { return Math.pow(d - yBar, 2); })
				.reduce(reduceSumFunc);
				
			var ssXY = xSeries.map(function(d, i) { return (d - xBar) * (ySeries[i] - yBar); })
				.reduce(reduceSumFunc);
				
			var slope = ssXY / ssXX;
			var intercept = yBar - (xBar * slope);
			var rSquare = Math.pow(ssXY, 2) / (ssXX * ssYY);
			
			return [slope, intercept, rSquare];
		}
}